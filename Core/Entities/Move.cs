﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Move
    {
        #region fields

        //TODO: KEF should these be actual nodes rather than ints??
        //The node that the peg starts from, and is removed after a move
        private int _startNode;
        //The node the starting peg jumps, and is rmeoved after a move
        private int _jumpNode;
        //The node that the starting peg fills, and is added after a move
        private int _endNode;

        #endregion

        #region properties

        public int StartNode
        {
            get { return _startNode; }
            set { _startNode = value; }
        }

        public int JumpNode
        {
            get { return _jumpNode; }
            set { _jumpNode = value; }
        }

        public int EndNode
        {
            get { return _endNode; }
            set { _endNode = value; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Constructor for a move
        /// </summary>
        /// <param name="startNode">The node that the peg starts from, and is removed after a move</param>
        /// <param name="jumpNode">The node the starting peg jumps, and is rmeoved after a move</param>
        /// <param name="endNode">The node that the starting peg fills, and is added after a move</param>
        public Move(int startNode, int jumpNode, int endNode)
        {
            _startNode = startNode;
            _jumpNode = jumpNode;
            _endNode = endNode;   
        }

        #endregion
    }
}
