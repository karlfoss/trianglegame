﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Node
    {
        #region fields

        private bool _hasPeg;
        // Represents the id of the node in it's array
        private int _indexId;
        private int _east;
        private int _west;
        private int _northEast;
        private int _northWest;
        private int _southEast;
        private int _southWest;

        public enum connectingNodes {East, West, NorthEast, NorthWest, SouthEast, SouthWest };

        #endregion

        #region properties

        /// <summary>
        /// Property used to set or retrieve if the current node has a peg or not
        /// </summary>
        public bool HasPeg
        {
            get { return _hasPeg; }
            set { _hasPeg = value; }
        }

        //TODO: KEF deteremine if this is needed
        public int IndexId
        {
            get { return _indexId; }
            set { _indexId = value; }
        }

        public int East
        {
            get { return _east; }
            set { _east = value; }
        }

        public int West
        {
            get { return _west; }
            set { _west = value; }
        }

        public int NorthEast
        {
            get { return _northEast; }
            set { _northEast = value; }
        }

        public int NorthWest
        {
            get { return _northWest; }
            set { _northWest = value; }
        }

        public int SouthEast
        {
            get { return _southEast; }
            set { _southEast = value; }
        }

        public int SouthWest
        {
            get { return _southWest; }
            set { _southWest = value; }
        }

        #endregion

        #region public methods

        public Node(bool hasPeg, int indexId)
        {
            _hasPeg = hasPeg;
            _indexId = indexId;
            _east = -1;
            _west = -1;
            _northEast = -1;
            _northWest = -1;
            _southEast = -1;
            _southWest = -1;
        }

        /// <summary>
        /// Constructure for Node, by default every node will have a peg
        /// and none of the nodes will be connected to each other (ie all references are null)
        /// TODO: KEF how redoudant is it to declare null values for below??? Make this more flexable instead??? And override with other combinations
        /// </summary>
        public Node(bool hasPeg, int indexId, int east, int west, int northEast, int northWest, int southEast, int southWest)
        {
            _hasPeg = hasPeg;
            _indexId = indexId;
            _east = east;
            _west = west;
            _northEast = northEast;
            _northWest = northWest;
            _southEast = southEast;
            _southWest = southWest;
        }

        #endregion

        #region private methods

        #endregion
    }
}
