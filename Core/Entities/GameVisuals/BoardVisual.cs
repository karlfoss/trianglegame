﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Core.Entities.Graphics;

namespace Core.Entities.GameVisuals
{
    /// <summary>
    /// Represents the visual object of a board, including the charaistics of the visual object
    /// along with all of the underlaying Board object that actually represents the game logics
    /// TODO: KEF should this simply inhereit from the Board instead???? ---> how would this work with setting the value of these objects of the parent class (ie number of nodes)
    /// 
    /// TODO: KEF condsider making this into a single object that contains both the logic and the visual settings
    ///        Chose the for the following reasons:
    ///        1 - acts as a wrapper so that other classes can use the Board class (ie Simulator has no use for a visual object)
    ///        2 - allows changes to the underlying object of Board that should make this more modular
    /// </summary>
    public class BoardVisual : Board
    {
        #region fields

        // Represents the current board
        private Board _gameBoard;

        // The current settings for the board visual
        private TrianglePictureBox _gameBoardPicture;

        #endregion

        #region properties

        /// <summary>
        /// 
        /// </summary>
        public Board gameBoard
        {
            get { return _gameBoard; }
            set { _gameBoard = value; }
        }

        public TrianglePictureBox gameBoardPicture
        {
            get { return _gameBoardPicture; }
            set { _gameBoardPicture = value; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Constructor that creates and inital board for simlular use and game play
        /// </summary>
        /// <param name="numberOfLevels"></param>
        /// <param name="startingPostionWithNoPeg"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        public BoardVisual(int numberOfLevels, int startingPostionWithNoPeg, int height, int width)
        {
            _gameBoard = new Board(NumberOfLevels: NumberOfLevels, startingPostionWithNoPeg: startingPostionWithNoPeg);
            _gameBoardPicture = new TrianglePictureBox(width, height);
            _gameBoardPicture.BackColor = Color.SaddleBrown; //TODO: replace with wood image
            _gameBoardPicture.Height = height;
            _gameBoardPicture.Width = width;
        }

        #endregion

        #region private methods

        #endregion
    }
}
