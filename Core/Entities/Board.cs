﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Board
    {
        #region fields

        //Represents the current board
        private List<Node> _nodes = new List<Node>();
        //Number of Levels in a triangle
        private int _numberOfLevels;
        //Number of Nodes in a triangle (aka holes)
        private int _numberOfNodes;
        private int _numberOfPegsRemaining;

        //All Nodes have a peg by default
        private const bool DEFAULT_PEG_VALUE = true;

        //Represents the first row/level of the game board (ie top of triangle)
        private const int FIRST_LEVEL = 1;

        #endregion

        #region properties

        /// <summary>
        /// 
        /// </summary>
        public int NumberOfPegsRemaining
        {
            get { return _numberOfPegsRemaining; }
            set { _numberOfPegsRemaining = value; }
        }

        public List<Node> Nodes
        {
            get { return _nodes; }
            set { _nodes = value; }
        }

        public int NumberOfLevels
        {
            get { return _numberOfLevels; }
            set { _numberOfLevels = value; }
        }

        public int NumberOfNodes
        {
            get { return _numberOfNodes; }
            set { _numberOfNodes = value; }
        }

        #endregion

        #region public methods

        public Board()
        {
            //use defaults
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nodes"></param>
        /// <param name="NumberOfLevels"></param>
        /// <param name="NumberOfNodes"></param>
        /// <param name="NumberOfPegsRemaining"></param>
        public Board(List<Node> Nodes, int NumberOfLevels, int NumberOfNodes, int NumberOfPegsRemaining)
        {
            _nodes = new List<Node>();
            foreach (Node node in Nodes)
            {
                Node copiedNode = new Node(node.HasPeg, node.IndexId, node.East, node.West, node.NorthEast, node.NorthWest, node.SouthEast, node.SouthWest);
                _nodes.Add(copiedNode);
            }

            _numberOfLevels = NumberOfLevels;
            _numberOfNodes = NumberOfNodes;
            _numberOfPegsRemaining = NumberOfPegsRemaining;
        }

        /// <summary>
        /// Constructor that creates an inital board to use
        /// </summary>
        /// <param name="NumberOfLevels"></param>
        /// <param name="startingPostionWithNoPeg"></param>
        public Board(int NumberOfLevels, int startingPostionWithNoPeg)
        {
            this.NumberOfLevels = NumberOfLevels;
            NumberOfNodes = calculateNumberOfNodes(NumberOfLevels);
            NumberOfPegsRemaining = NumberOfNodes - 1;
            Nodes = new List<Node>();

            // Create all of the Nodes
            for (int i = 0; i < NumberOfNodes; i++)
            {
                Node newNode = new Node(DEFAULT_PEG_VALUE, i);
                Nodes.Add(newNode);
            }

            int currentNodeIndex = 0;
            // Link all of the Nodes
            for (int levelNumber = FIRST_LEVEL; levelNumber <= NumberOfLevels; levelNumber++)
            {
                for (int nodeColumn = 1; nodeColumn <= levelNumber; nodeColumn++)
                {
                    //Check if the node is before the last column (thus having a link to the East)
                    if (nodeColumn < levelNumber && levelNumber != FIRST_LEVEL)
                    {
                        Nodes[currentNodeIndex].East = currentNodeIndex + 1;
                    }

                    //Check if the node is after the first column (thus having a link to the West)
                    if (nodeColumn > 1 && levelNumber != FIRST_LEVEL)
                    {
                        Nodes[currentNodeIndex].West = currentNodeIndex - 1;
                    }

                    //Check if we are above the last level (thus having SouthEast and SouthWest links)
                    if (levelNumber != NumberOfLevels)
                    {
                        Nodes[currentNodeIndex].SouthWest = currentNodeIndex + levelNumber;
                        Nodes[currentNodeIndex].SouthEast = currentNodeIndex + levelNumber + 1;
                    }

                    //Check if we are after the first level + it's after the first column (thus having a NorthWest link)
                    if (levelNumber > FIRST_LEVEL && nodeColumn > 1)
                    {
                        Nodes[currentNodeIndex].NorthWest = currentNodeIndex - levelNumber;

                    }

                    //Check if we are after the first level + it's the not the last column (thus having a NorthEast link)
                    if (levelNumber > FIRST_LEVEL && nodeColumn < levelNumber)
                    {
                        Nodes[currentNodeIndex].NorthEast = currentNodeIndex - levelNumber + 1;
                    }

                    //Go to the next node
                    currentNodeIndex++;
                }
            }

            //Remove the peg from the first location
            Nodes[startingPostionWithNoPeg].HasPeg = false;
        }

        public static int calculateNumberOfNodes(int NumberOfLevels)
        {
            int totalNumberOfNodes = 0;

            for (int i = 1; i <= NumberOfLevels; i++)
            {
                totalNumberOfNodes += i;
            }

            return totalNumberOfNodes;
        }

        //TODO: KEF find a way to clean up these checks if possible - Make directions into enums and constants (thus allowing numbers to be unsed in place of repative numbers (put all connecting Nodes into an array and use enum to fetch)
        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findEastMove(Node currentNode)
        {
            Move possibleMove = null;
            
            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            if (currentNode.East == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.East];

            if (jumpNode.East == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.East];

            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }

        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findWestMove(Node currentNode)
        {
            Move possibleMove = null;

            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            if (currentNode.West == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.West];

            if (jumpNode.West == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.West];

            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }

        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findNorthEastMove(Node currentNode)
        {
            Move possibleMove = null;

            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            if (currentNode.NorthEast == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.NorthEast];

            if (jumpNode.NorthEast == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.NorthEast];

            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }

        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findNorthWestMove(Node currentNode)
        {
            Move possibleMove = null;

            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            if (currentNode.NorthWest == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.NorthWest];

            if (jumpNode.NorthWest == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.NorthWest];

            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }

        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findSouthEastMove(Node currentNode)
        {
            Move possibleMove = null;

            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            if (currentNode.SouthEast == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.SouthEast];

            if (jumpNode.SouthEast == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.SouthEast];

            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }


        /// <summary>
        /// Checks if there is a valid jump move in the specified direction
        /// </summary>
        /// <returns>Returns a move if it is possible, null otherwise</returns>
        public Move findSouthWestMove(Node currentNode)
        {
            Move possibleMove = null;

            // If the current node doesn't have a peg return null
            if (!currentNode.HasPeg)
            {
                return null;
            }

            // If there is no connection to the next node, return null
            if (currentNode.SouthWest == -1)
            {
                return null;
            }

            Node jumpNode = Nodes[currentNode.SouthWest];

            // If there is no connection to the node after the next node, return null
            if (jumpNode.SouthWest == -1)
            {
                return null;
            }

            Node endNode = Nodes[jumpNode.SouthWest];

            // There must be a peg to jump, and an empty space to jump to
            if (jumpNode.HasPeg && !endNode.HasPeg)
            {
                possibleMove = new Move(currentNode.IndexId, jumpNode.IndexId, endNode.IndexId);
            }
            return possibleMove;
        }

        #endregion

        #region private methods

        #endregion
    }
}
