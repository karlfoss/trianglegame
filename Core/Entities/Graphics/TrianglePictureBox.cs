﻿/**
 * Retrieved 01/02/2018
 * Source of this code: (and proof why I should use a game engine instead) 
 * https://stackoverflow.com/questions/35467199/is-this-possible-to-have-triangular-picturebox-instead-of-the-rectangular-one
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Core.Entities.Graphics
{
    /// <summary>
    /// Triangle object that takes the 
    /// TODO: KEF don't ever use this mess. At worst, simply import a picture that represents a picturebox
    /// </summary>
    public class TrianglePictureBox : PictureBox
    {
        /// <summary>
        /// Constructor that takes the width and height of the screen
        /// TODO: KEF make multiple constructors to support a vertical screen instead
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public TrianglePictureBox(int width, int height)
        {
            // Sync the width with the height
            this.Width = width;
            // Center the triangle in the middle of the screen
            this.Height = height / 10;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            using (var p = new GraphicsPath())
            {
                //int width = 2560;
                //int height = 1440;
                ///p.AddPolygon(new Point[] {
                ///new Point(this.Width / 2, 0),
                ///new Point(0, Height),
                ///new Point(Width, Height) });

                p.AddPolygon(new Point[] {
                new Point(this.Width / 2, 0),
                new Point(0, this.Height),
                new Point(this.Width, this.Height) });

                this.Region = new Region(p);
                base.OnPaint(pe);
            }
        }
    }
}
