﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Core.Entities
{
    public class Sequence
    {
        #region fields

        private List<Move> _moves = new List<Move>();
        private int _numberOfPegsRemaining;

        #endregion

        #region properties

        //TODO: KEF will the set work as expected with this list??
        //TODO: KEF update the set to create a completely new object
        public List<Move> Moves
        {
            get { return _moves; }
            set { _moves = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int numberOfPegsRemaining
        {
            get { return _numberOfPegsRemaining; }
            set { _numberOfPegsRemaining = value; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Constructor for a sequence
        /// </summary>
        public Sequence()
        {
            _moves = new List<Move>();
        }

        /// <summary>
        /// Constructor for a sequence with existing moves
        /// TODO: KEF ensure this doesn't cross reference with the old sequence
        /// Note: This should use the clonable interface, as it is using the prototype design pattern to copy an object
        /// </summary>
        public Sequence(List<Move> moves)
        {
            _moves = new List<Move>();
            foreach (Move move in moves)
            {
                Move copiedMove = new Move(move.StartNode, move.JumpNode, move.EndNode);
                _moves.Add(copiedMove);
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Series of Moves\n");
            for (int i = 0; i < Moves.Count; i++)
            {
                stringBuilder.Append("Move Number " + (i + 1) + ": Start Position: " + (Moves[i].StartNode + 1) + " Jump Position: " + (Moves[i].JumpNode + 1) + " End Position: " + (Moves[i].EndNode + 1) + "\n");
            }
            stringBuilder.Append("End of Series of Moves\n");
            
            return stringBuilder.ToString();
        }

        #endregion
    }
}
