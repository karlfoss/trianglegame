﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Simulator;

namespace UnitTest
{
    [TestClass]
    public class CoreTests
    {
        private const int DEFAULT_NUMBER_OF_LEVELS = 5;

        private const int ONE_PEG_REMAINING = 1;

        private const int EXPECTED_NUMBER_OF_SOLUTIONS_FOR_POSITION0_WITH_ONE_PEG_REMAINING = 29760;
        private const int EXPECTED_NUMBER_OF_SOLUTIONS_FOR_POSITION4_WITH_ONE_PEG_REMAINING = 1550;

        [TestMethod]
        public void checkExpectedOutComesForStartingPosition0()
        {
            Simulation triangleWithNoPegInPosition0 = new Simulation(DEFAULT_NUMBER_OF_LEVELS, 0);

            Assert.IsTrue(triangleWithNoPegInPosition0.numberOfPegsRemaining[ONE_PEG_REMAINING] == EXPECTED_NUMBER_OF_SOLUTIONS_FOR_POSITION0_WITH_ONE_PEG_REMAINING);
        }

        [TestMethod]
        public void checkExpectedOutComesForStartingPosition4()
        {
            Simulation triangleWithNoPegInPosition4 = new Simulation(DEFAULT_NUMBER_OF_LEVELS, 4);

            Assert.IsTrue(triangleWithNoPegInPosition4.numberOfPegsRemaining[ONE_PEG_REMAINING] == EXPECTED_NUMBER_OF_SOLUTIONS_FOR_POSITION4_WITH_ONE_PEG_REMAINING);
        }

        //TODO: KEF have a test for default board creation

        //TODO: KEF have a test that checks for new sequence creation, new board creation, new node creation (to test for cross references

        //TODO: KEF have one that checks the solved number of moves
    }
}
