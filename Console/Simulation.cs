﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Core.Entities;

namespace Simulator
{
    ///ENHANCEMENTS
    ///Add validation everyone (board, valid moves, node, etc
    ///Build a GUI with either windows or https://stackoverflow.com/questions/10055797/c-sharp-simple-game-architecture
    ///             -Have it auto generate boards based on number of levels
    ///             -Use Simulator on the back-end (for hints and such) move entities to the Core
    ///             -Have a move be selected (start highlighted) then select end node, validate it, and then do a refreash (check all nodes for peg)

    /// <summary>
    /// 
    /// </summary>
    public class Simulation
    {
        #region fields

        //Holds all possible move sequences and how many pegs we are left with once the sequence is complete
        private List<Sequence> _sequences = new List<Sequence>();
        private List<Sequence> _winningSequences = new List<Sequence>();
        private int[] _numberOfPegsRemaining = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        private int _startingPositionOfLastSimulation = -1;

        //TODO: update these as they are no longer constants and should be treated like normal variables
        private int MINIMUM_NUMBER_OF_PEGS_REQUIRED = 2;
        private int MISSING_PEG_POSITION = 0;
        private int NUMBER_OF_LEVELS = 5;

        #endregion

        #region properties

        public List<Sequence> Sequences
        {
            get { return _sequences; }
            set { _sequences = value; }
        }

        public List<Sequence> WinningSequences
        {
            get { return _winningSequences; }
            set { _winningSequences = value; }
        }

        public int[] NumberOfPegsRemaining
        {
            get { return _numberOfPegsRemaining; }
            set { _numberOfPegsRemaining = value; }
        }

        public int StartingPositionOfLastSimulation
        {
            get { return _startingPositionOfLastSimulation; }
            set { _startingPositionOfLastSimulation = value; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Simulation simulation = new Simulation();
            // Start the simulation by prompting user for input
            simulation.PromptUserForInput();
        }

        public Simulation()
        {
            //TODO: make a separate class for the actual Simulation and leave this one as a console to allow for separation of concerns
            //TODO: make a constructor for another class to use (IE, number of levels, starting position, etc)
            //TODO: this could also be made into an API to compute and return this information
        }

        #endregion

        #region private methods

        private void PromptUserForInput()
        {
            bool exitGame = false;

            do
            {
                Console.Clear();
                Console.WriteLine("Starting Position: " + (MISSING_PEG_POSITION + 1));
                Console.WriteLine("Number of Pyramid Levels: " + NUMBER_OF_LEVELS);
                Console.WriteLine("Number of Pegs Required: " + MINIMUM_NUMBER_OF_PEGS_REQUIRED);
                // Prompt the user for options
                Console.WriteLine("Options for Triangle Game Solver:");
                Console.WriteLine("\t1 - Find All Solutions");
                Console.WriteLine("\t2 - Change Triangle Game Settings");
                Console.WriteLine("\t3 - View Solutions From the Last Simulation");
                Console.WriteLine("\t4 - Exit");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        Sequences = new List<Sequence>();
                        WinningSequences = new List<Sequence>();
                        for (int i = 0; i < NumberOfPegsRemaining.Count(); i++)
                        {
                            NumberOfPegsRemaining[i] = 0;
                        }
                        StartingPositionOfLastSimulation = MISSING_PEG_POSITION;
                        //use this to initialize any simulation settings
                        RunSimulation(NUMBER_OF_LEVELS, MISSING_PEG_POSITION);
                        Console.WriteLine("Press any key to continue.....");
                        Console.ReadKey();
                        break;
                    case "2":
                        ChangeSimulationSettings();
                        break;
                    case "3":
                        // Add new logic flow after here
                        if (Sequences == null || Sequences.Count < 1)
                        {
                            Console.WriteLine("Please run the 'Find All Solutions' command before viewing solutions");
                            Console.WriteLine("Press any key to continue.....");
                            Console.ReadKey();
                            break;
                        }
                        ViewAllSolutions();
                        break;
                    case "4":
                        exitGame = true;
                        break;
                }

            } while (!exitGame);

            Console.WriteLine("Thanks for playing!");
            System.Threading.Thread.Sleep(2000);
        }

        private void ChangeSimulationSettings()
        {
            bool exitGameSettings = false;

            do
            {
                // Prompt the user for options
                Console.Clear();
                Console.WriteLine("Starting Position: " + (MISSING_PEG_POSITION + 1));
                Console.WriteLine("Number of Pyramid Levels: " + NUMBER_OF_LEVELS);
                Console.WriteLine("Number of Pegs Required: " + MINIMUM_NUMBER_OF_PEGS_REQUIRED);
                Console.WriteLine("--------------------------");
                Console.WriteLine("Update Triangle Game Simulator:");
                Console.WriteLine("\t1 - Change Starting Position of Missing Peg");
                Console.WriteLine("\t2 - Exit Settings Changes");
                //Console.WriteLine("\t2 - Change Number of Pyramid Levels");
                //Console.WriteLine("\t3 - Change Minimum Number of Pegs Required");
                Console.Write("Your option: ");

                // Use a switch statement to do the math.
                switch (Console.ReadLine())
                {
                    case "1":
                        // Have user enter the hole that won't have a peg in it (aka starting position)
                        MISSING_PEG_POSITION = GetStartingPositionInput("Starting Position of the Missing Peg");
                        break;
                    case "2":
                        exitGameSettings = true;
                        break;
                }

            } while (!exitGameSettings);
        }

        private static int GetStartingPositionInput(string option)
        {
            Console.WriteLine("Please Enter the " + option);
            int optionChange = 0;
            bool isValidStartingPosition = false;

            do
            {
                while (!int.TryParse(Console.ReadLine(), out optionChange))
                {
                    Console.WriteLine("Please enter a valid integer!");
                }

                // TOOD: calculate the number of possible starting positions depending on the number of levels
                //TODO: make a constant/variable for these values
                if (optionChange >= 1 && optionChange <= 15)
                {
                    isValidStartingPosition = true;
                }
                else
                {
                    Console.WriteLine("Please enter a valid staring position! It must be greater than 0 and less than 16.");
                }

            } while (!isValidStartingPosition);

            // TODO: the user only sees the numbers 1-15, we subtract 1 here because our algorithm uses a base 0 approach
            return optionChange - 1;
        }

        private void ViewAllSolutions()
        {
            bool exitSolutionViewer = false;

            do
            {
                // Prompt the user for options
                Console.Clear();
                Console.WriteLine("Starting Position (from the last run): " + (StartingPositionOfLastSimulation + 1));
                Console.WriteLine("Number of Solutions " + WinningSequences.Count);
                Console.WriteLine("Number of Possible Outcomes: " + Sequences.Count);
                Console.WriteLine("--------------------------");
                Console.WriteLine("Triangle Game Solutions (from the last run):");
                Console.WriteLine("\t0 - Exit");
                Console.WriteLine("\tEnter a number between 1 and " + WinningSequences.Count + " to view a solution");
                Console.Write("Your option: ");

                String userInput = Console.ReadLine();
                int solutionToView;

                if (int.TryParse(userInput, out solutionToView))
                {
                    if (solutionToView == 0)
                    {
                        exitSolutionViewer = true;
                    }
                    else if (solutionToView >= 1 && solutionToView <= WinningSequences.Count)
                    {
                        Console.WriteLine(WinningSequences[solutionToView - 1].ToString());
                        Console.WriteLine("Press any key to continue.....");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("Please enter an integer between 1 and " + WinningSequences.Count + " to view a solution");
                        Console.WriteLine("Press any key to continue.....");
                        Console.ReadKey();
                    }

                }
                else
                {
                    Console.WriteLine("Please enter a valid integer between 1 and " + WinningSequences.Count + " to view a solution");
                    Console.WriteLine("Press any key to continue.....");
                    Console.ReadKey();
                }

            } while (!exitSolutionViewer);
        }

        private void RunSimulation(int numberOfLevels, int startingPostionWithNoPeg)
        {
            Board triangle = new Board();

            triangle = new Board(numberOfLevels, startingPostionWithNoPeg);

            Jump(triangle);

            //Counts the distribution of pegs remaining from all possible sequences
            foreach (Sequence sequence in Sequences)
            {
                NumberOfPegsRemaining[sequence.numberOfPegsRemaining]++;
                if (sequence.numberOfPegsRemaining == 1)
                {
                    WinningSequences.Add(sequence);
                }
            }
            Console.WriteLine("A total of " + Sequences.Count + " sequences were analyzed");
            Console.WriteLine("A total of " + WinningSequences.Count + " winning sequences were discovered");
            for (int i = 1; i < NumberOfPegsRemaining.Count(); i++)
            {
                Console.WriteLine("The number of sequences with " + i + " pegs remaining: " + NumberOfPegsRemaining[i]);
            }
        }

        /// <summary>
        /// All initial moves found
        /// </summary>
        /// <param name="currentBoard"></param>
        /// <param name="currentSequence"></param>
        private void Jump(Board currentBoard)
        {
            Sequence firstSequence = new Sequence();
            Sequences.Add(firstSequence);

            List<Move> movesDoneBeforeSplit = new List<Move>();

            //Try all available moves
            List<Move> allPossibleMoves = FindAllMoves(currentBoard);
            for (int i = 0; i < allPossibleMoves.Count; i++)
            {
                // Only create a new sequence if there is more than 1 move to do
                if (i == 0)
                {
                    Jump(currentBoard, firstSequence, allPossibleMoves[i]);
                }

                if (i > 0)
                {
                    Sequence newSequence = new Sequence(movesDoneBeforeSplit);
                    Sequences.Add(newSequence);
                    Jump(currentBoard, newSequence, allPossibleMoves[i]);
                }
            }
        }

        /// <summary>
        /// helper method
        /// </summary>
        /// <param name="currentBoard"></param>
        /// <param name="currentSequence"></param>
        /// <param name="moveChosen"></param>
        private void Jump(Board currentBoard, Sequence currentSequence, Move moveChosen)
        {
            //make a move TODO: KEF might have to create a new board each time a move is made...how to copy without making references
            currentSequence.Moves.Add(moveChosen);
            Board updatedBoard = Move(currentBoard, moveChosen);

            //Check if there are more pegs to jump (this should probably be done before making call
            if (updatedBoard.NumberOfPegsRemaining < MINIMUM_NUMBER_OF_PEGS_REQUIRED)
            {
                currentSequence.numberOfPegsRemaining = updatedBoard.NumberOfPegsRemaining;
                return;
            }

            //Check if any possible moves remain
            List<Move> allPossibleMoves = FindAllMoves(updatedBoard);
            if (allPossibleMoves == null || allPossibleMoves.Count == 0)
            {
                currentSequence.numberOfPegsRemaining = updatedBoard.NumberOfPegsRemaining;
                return;
            }

            List<Move> movesDoneBeforeSplit = new List<Move>();
            //Create list of all current moves because of potentially splits
            foreach (Move move in currentSequence.Moves)
            {
                Move copiedMove = new Move(move.StartNode, move.JumpNode, move.EndNode);
                movesDoneBeforeSplit.Add(copiedMove);
            }

            //Try all available moves
            for (int i = 0; i < allPossibleMoves.Count; i++)
            {
                // Only create a new sequence if there is more than 1 move to dos
                if (i == 0)
                {
                    Jump(updatedBoard, currentSequence, allPossibleMoves[i]);
                }

                if (i > 0)
                {
                    Sequence newSequence = new Sequence(movesDoneBeforeSplit);
                    Sequences.Add(newSequence);
                    Jump(updatedBoard, newSequence, allPossibleMoves[i]);
                }
            }
        }

        private Board Move(Board currentBoard, Move move)
        {
            Board updatedBoard = new Board(currentBoard.Nodes, currentBoard.NumberOfLevels, currentBoard.NumberOfNodes, currentBoard.NumberOfPegsRemaining);
            // Remove pegs from Start and Jump, add a peg to the end
            updatedBoard.Nodes[move.StartNode].HasPeg = false;
            updatedBoard.Nodes[move.JumpNode].HasPeg = false;
            updatedBoard.Nodes[move.EndNode].HasPeg = true;
            // Remove a peg
            updatedBoard.NumberOfPegsRemaining--;

            return updatedBoard;
        }

        private List<Move> FindAllMoves(Board currentBoard)
        {
            List<Move> allPossibleMoves = new List<Move>();

            for (int i = 0; i < currentBoard.NumberOfNodes; i++)
            {
                List<Move> movesFromCurrentNode = new List<Move>();
                //TODO: KEF consider using actual nodes instead of index ids inside of moves, and having a boolean returned rather than a move
                movesFromCurrentNode.Add(currentBoard.findEastMove(currentBoard.Nodes[i]));
                movesFromCurrentNode.Add(currentBoard.findWestMove(currentBoard.Nodes[i]));
                movesFromCurrentNode.Add(currentBoard.findNorthEastMove(currentBoard.Nodes[i]));
                movesFromCurrentNode.Add(currentBoard.findNorthWestMove(currentBoard.Nodes[i]));
                movesFromCurrentNode.Add(currentBoard.findSouthEastMove(currentBoard.Nodes[i]));
                movesFromCurrentNode.Add(currentBoard.findSouthWestMove(currentBoard.Nodes[i]));

                foreach (Move move in movesFromCurrentNode)
                {
                    // Only add allowed moves to the list of possible moves
                    if (move != null)
                    {
                        allPossibleMoves.Add(move);
                    }
                }
            }

            return allPossibleMoves;
        }

        #endregion
    }
}
