﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Core.Entities.GameVisuals;

namespace Game
{
    public partial class Game : Form
    {
        /**********************************************
            1. Convert this into an mvc application (see https://stackoverflow.com/questions/23775849/how-to-handle-game-flow) Use pluralsight and tuturialspoint for studying
            2. Look into improving the overall gameflow (see https://codereview.stackexchange.com/questions/127515/first-c-program-snake-game) 
            3. Find a game engine to use....probably Untity https://gamedev.stackexchange.com/questions/109/what-c-libraries-can-be-used-to-support-game-development
            4. Overview to use Unity with EC/MVC pattern: https://www.toptal.com/unity-unity3d/unity-with-mvc-how-to-level-up-your-game-development
            5. Guide to implementing Unity into visual studio https://docs.unity3d.com/Manual/VisualStudioIntegration.html
        ***************************************************/


        public Game()
        {
            InitializeComponent();
        }

        private void Game_Load(object sender, EventArgs e)
        {
            //TODO: KEF screw making custom size picture boxes. Just use images with missing sections to look like it isn't sqaure

            //Create the inital screen, make this setting more modular (changes based on screen size), and custamizable by a user
            //TODO: Auto fit logic, make enhancement to use this
            //this.Width = Screen.PrimaryScreen.Bounds.Width;
            //this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Width = 2560;
            this.Height = 1440;

            this.BackColor = Color.ForestGreen;

            //Set up the default game board
            ResetGameBoard();       
        }

        private void ResetGameBoard()
        {
            //TODO: KEF the control board takes up the entire screen. This could present problems with displaying other controls
            BoardVisual gameBoard = new BoardVisual(5, 0, this.Height, this.Width);
            //gameBoard.findNorthWestMove(gameBoard.);
            this.Controls.Add(gameBoard.gameBoardPicture);
        }
    }
}
