# Overview #

This is a simulator that was built to find every possible solution to the Triangle Peg Game. This link (https://www.wikihow.com/Win-the-Peg-Game) goes over 
the rules and gives an example of how to solve the game. 

All but one of the 15 positions in the below image will be filled with a peg. The goal is to 'jump' over and remove all other pegs. A winning solution is where there
is only one peg remaining. The game can be solved from any starting position (IE the position without a peg in it). There are actually 1000s of possible solutions
and this program should be able to find every one of them.

![Image of Triangle Game Board](https://www.joenord.com/puzzles/peggame/Peg_Board_Game_files/image001.gif)

### How To Use ###

1. Clone this git repository
2. Run the code with the **Simulator** project as the startup project
3. The current default start position is 1 in the above image. This can be changed in the command line before running the simulation.
4. After running it you will be able to view all known solutions that are found.

## Future Enhancements ##
1. Adding a command line interface to chose the starting position
2. Printing off more stats like the number of possible solutions vs the number of possible outcomes
3. Making it easier to view/export all of the possible solutions.